from django.contrib import admin
from app.models import Person

# Register your models here.
class PersonAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'email']

admin.site.register(Person, PersonAdmin)