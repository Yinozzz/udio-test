from django.forms import ModelForm
from app.models import Person
from django import forms

class PersonForm(ModelForm):
    class Meta:
        model = Person
        fields = '__all__'
        widgets = {
            "name": forms.TextInput,
            "email": forms.EmailInput
        }


class SearchForm(forms.Form):
    search = forms.TextInput

