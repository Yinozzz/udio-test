from django.db import models

# Create your models here.
class Person(models.Model):
    name = models.CharField(verbose_name='name', max_length=200)
    email = models.EmailField(verbose_name='email', unique=True, blank=False)
