import json

from django.shortcuts import render, HttpResponse, get_object_or_404
from django.views import generic
from .models import Person
from .form import PersonForm, SearchForm
from django.db.models import Q

def person(request):
    person = Person.objects.all()
    if request.method == 'POST':
        form = SearchForm(request.POST)
        if form.is_valid():
            search = form.data.get('search')
            print(search)
            person_search = Person.objects.filter(Q(name=search)|Q(email=search))
            if person_search:
                return render(request, 'person.html', {'person': person_search})
            else:
                return render(request, 'person.html', {'person': person, 'context': 'No person matching'})
    return render(request, 'person.html', {'person': person})


def personAdd(request):
    context = 'Please input information'
    form = PersonForm()
    person = Person.objects.all()
    if request.method == 'POST':
        formnew = PersonForm(request.POST)
        if formnew.is_valid():
            personName = request.POST.get('name')
            personEmail = request.POST.get('email')
            personadd = Person(name=personName, email=personEmail)
            personadd.save()
            context = 'success'
        return render(request, 'personAdd.html', {'form': form, 'context': context})
    else:
        return render(request, 'personAdd.html', {'form': form, 'context': context})

def personjs(request):
    person = Person.objects.all()
    # if request.method == 'POST':
    #     form = SearchForm(request.POST)
    #     if form.is_valid():
    #         search = form.data.get('search')
    #         print(search)
    #         person_search = Person.objects.filter(Q(name=search) | Q(email=search))
    #         if person_search:
    #             print(HttpResponse(person_search))
    #             return HttpResponse(person_search)
    #         else:
    #             return render(request, 'personjs.html', {'person': person, 'context': 'No person matching'})
    # return render(request, 'personjs.html', {'person': person})
    if request.method == 'POST':
        search = request.POST.get('search')
        queryset = Person.objects.filter(Q(name=search) | Q(email=search)).first()
        result = dict()
        if queryset:
            result['id'] = queryset.id
            result['name'] = queryset.name
            result['email'] = queryset.email
        return HttpResponse(json.dumps(result))
    else:
        return render(request, 'personjs.html', {'person': person})






